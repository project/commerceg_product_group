<?php

namespace Drupal\commerceg_product_group\Membership;

use Drupal\commerceg\MachineName\Plugin\GroupContentEnabler;
use Drupal\commerceg_product_group\MachineName\Bundle\Group as GroupBundle;
use Drupal\commerceg_product_group\MachineName\ConfigEntity\GroupRole;
use Drupal\commerceg_product_group\MachineName\Plugin\GroupContentEnabler as ProductGroupGroupContentEnabler;

use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupContentInterface;
use Drupal\group\Entity\GroupContentTypeInterface;
use Drupal\group\Entity\GroupTypeInterface;
use Drupal\group\GroupMembershipLoaderInterface;
use Drupal\user\UserInterface;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Default implementation of the Membership Manager.
 */
class Manager implements ManagerInterface {

  /**
   * The membership loader.
   *
   * @var \Drupal\group\GroupMembershipLoaderInterface
   */
  protected $membershipLoader;

  /**
   * The group content storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupContentStorageInterface
   */
  protected $contentStorage;

  /**
   * The group content type storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupContentTypeStorageInterface
   */
  protected $contentTypeStorage;

  /**
   * The Group Customer role entity.
   *
   * @var \Drupal\group\Entity\GroupRoleInterface
   */
  protected $customerRole;

  /**
   * Constructs a new Manager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\group\GroupMembershipLoaderInterface $membership_loader
   *   The group membership loader.
   *
   * @throws \InvalidArgumentException
   *   If the group customer role used to give access to users in product groups
   *   via membership in other groups does not exist.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    GroupMembershipLoaderInterface $membership_loader
  ) {
    $this->membershipLoader = $membership_loader;

    $this->contentStorage = $entity_type_manager->getStorage('group_content');
    $this->contentTypeStorage = $entity_type_manager
      ->getStorage('group_content_type');

    $this->customerRole = $entity_type_manager
      ->getStorage('group_role')
      ->load(GroupRole::GROUP_CUSTOMER);

    // The Group Customer role is required for the Managed Memberships feature
    // to work. It is installed upon module installation; if somehow it was
    // removed, something is wrong.
    if (!$this->customerRole) {
      throw new \RuntimeException(
        'The group customer group role does not exist on the %s group type.',
        GroupBundle::PRODUCTS
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function ensureMembershipsForAssociatedGroup(
    GroupInterface $product_group,
    GroupInterface $associated_group
  ) {
    if ($product_group->bundle() !== GroupBundle::PRODUCTS) {
      throw new \InvalidArgumentException(
        'The group that users will be added to must be of %s group type, %s given.',
        GroupBundle::PRODUCTS,
        $product_group->bundle()
      );
    }

    $memberships = $this->membershipLoader->loadByGroup($associated_group);
    foreach ($memberships as $membership) {
      $this->ensureMembership($membership->getUser(), $product_group);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function ensureMembershipsForUser(
    UserInterface $user,
    GroupInterface $associated_group
  ) {
    // Add memberships of the user in all product groups associated
    // with the given group.
    $product_groups = $this->getAssociatedProductGroups($associated_group);
    foreach ($product_groups as $product_group) {
      $this->ensureMembership(
        $user,
        $product_group
      );
    };
  }

  /**
   * {@inheritdoc}
   */
  public function removeMembershipsForAssociatedGroup(
    GroupInterface $product_group,
    GroupInterface $associated_group
  ) {
    if ($product_group->bundle() !== GroupBundle::PRODUCTS) {
      throw new \InvalidArgumentException(
        'The group that users will be added to must be of %s group type, %s given.',
        GroupBundle::PRODUCTS,
        $product_group->bundle()
      );
    }

    $memberships = $this->membershipLoader->loadByGroup($associated_group);
    foreach ($memberships as $membership) {
      $this->removeMembership(
        $membership->getUser(),
        $product_group,
        $associated_group
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function removeMembershipsForUser(
    UserInterface $user,
    GroupInterface $associated_group
  ) {
    // Remove any memberships of the user from all product groups associated
    // with the given group.
    $product_groups = $this->getAssociatedProductGroups($associated_group);
    foreach ($product_groups as $product_group) {
      $this->removeMembership(
        $user,
        $product_group,
        $associated_group
      );
    };
  }

  /**
   * {@inheritdoc}
   */
  public function ensureMembership(
    UserInterface $user,
    GroupInterface $product_group
  ) {
    $content_type = $this->getContentType(
      $product_group->getGroupType(),
      GroupContentEnabler::MEMBERSHIP
    );
    if (!$content_type) {
      throw new \Exception(sprintf(
        'Cannot add the user with ID %s to the product group with ID %s because the %s group content enabler plugin is not installed on the group type.',
        $user->id(),
        $group->id(),
        GroupContentEnabler::MEMBERSHIP
      ));
    }

    $content = $this->getProductGroupMembershipContent(
      $user,
      $product_group,
      $content_type,
      TRUE
    );
    if (!$content) {
      return;
    }

    $this->ensureRole($content);
  }

  /**
   * {@inheritdoc}
   */
  public function removeMembership(
    UserInterface $user,
    GroupInterface $product_group,
    GroupInterface $associated_group
  ) {
    $content_type = $this->getContentType(
      $product_group->getGroupType(),
      GroupContentEnabler::MEMBERSHIP
    );
    if (!$content_type) {
      throw new \Exception(sprintf(
        'Cannot add the user with ID %s to the product group with ID %s because the %s group content enabler plugin is not installed on the group type.',
        $user->id(),
        $group->id(),
        GroupContentEnabler::MEMBERSHIP
      ));
    }

    $content = $this->getProductGroupMembershipContent(
      $user,
      $product_group,
      $content_type,
      FALSE
    );
    if (!$content) {
      return;
    }

    $has_other_memberships = $this->hasOtherAssociatedGroupMemberships(
      $user,
      $product_group,
      $associated_group
    );
    if ($has_other_memberships) {
      return;
    }

    $this->removeRole($content);

  }

  /**
   * Returns whether the user has memberships in other associated groups.
   *
   * When removing a product group membership because the association between
   * the product group and an associated group has been removed, we do not want
   * to remove the membership if the user has a membership in one or more
   * other associated groups.
   *
   * This method returns whether the user has at least one membership on another
   * group other than the given associated group but of the same type, that is
   * also associated with the given product group.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to check the membership for.
   * @param \Drupal\group\Entity\GroupInterface $product_group
   *   The product group.
   * @param \Drupal\group\Entity\GroupInterface $associated_group
   *   The associated group to exclude when looking for existing memberships.
   *
   * @I Maybe it's more optimal to search for user-to-parent contents first; a
   *    user would more commonly belong to one or few parent groups, while a
   *    product group might be added to plenty of parent groups.
   */
  protected function hasOtherAssociatedGroupMemberships(
    UserInterface $user,
    GroupInterface $product_group,
    GroupInterface $associated_group
  ) {
    $association_content_type = $this->getContentType(
      $associated_group->getGroupType(),
      ProductGroupGroupContentEnabler::PRODUCTS
    );
    if (!$association_content_type) {
      throw new \RuntimeException(
        'The %s group content enabler plugin is not installed on the %s group type.',
        ProductGroupGroupContentEnabler::PRODUCTS,
        $associated_group->bundle()
      );
    }

    // Get group contents of the product group to other associated groups
    // i.e. excluding the given associated group.
    $association_content_ids = $this->contentStorage
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', $association_content_type->id())
      ->condition('entity_id', $product_group->id())
      ->condition('gid', $associated_group->id(), '!=')
      ->execute();
    if (!$association_content_ids) {
      return FALSE;
    }

    $membership_content_type = $this->getContentType(
      $associated_group->getGroupType(),
      GroupContentEnabler::MEMBERSHIP
    );
    $association_contents = $this->contentStorage->loadMultiple(
      $association_content_ids
    );

    // Check if the user has membership to at least one other associated group.
    foreach ($association_contents as $content) {
      $count = $this->contentStorage
        ->getQuery()
        ->accessCheck(FALSE)
        ->condition('type', $membership_content_type->id())
        ->condition('entity_id', $user->id())
        ->condition('gid', $content->getGroup()->id())
        ->count()
        ->execute();
      if ($count) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Returns the group content type for the given group type and plugin ID.
   *
   * @param \Drupal\group\Entity\GroupTypeInterface $group_type
   *   The group type to get the content type for.
   * @param string $plugin_id
   *   The plugin ID to get the content type for.
   *
   * @return \Drupal\group\Entity\GroupContentTypeInterface|null
   *   The group content type for the given group type and plugin, or NULL if
   *   none was found.
   */
  protected function getContentType(
    GroupTypeInterface $group_type,
    $plugin_id
  ) {
    $content_types = $this->contentTypeStorage
      ->loadByGroupType($group_type);
    foreach ($content_types as $content_type) {
      if ($content_type->getContentPluginId() !== $plugin_id) {
        continue;
      }

      return $content_type;
    }
  }

  /**
   * Returns the membership group content for the given user and product group.
   *
   * Optionally, create a group content if it doesn't exist. Note that the group
   * content entity is returned unsaved.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The product group.
   * @param \Drupal\group\Entity\GroupContentTypeInterface $content_type
   *   The group content type that associates products groups with users.
   * @param bool $create
   *   TRUE to create a group content if it doesn't exist i.e. associate the
   *   user with the product group, FALSE otherwise.
   *
   * @return \Drupal\group\Entity\GroupContentInterface|null
   *   The group content that associates the given user with the given product
   *   group, or NULL if it doesn't exist and `$create` is set to FALSE.
   *
   * @throws \RuntimeException
   *   If more than one existing group content was found.
   */
  protected function getProductGroupMembershipContent(
    UserInterface $user,
    GroupInterface $group,
    GroupContentTypeInterface $content_type,
    $create
  ) {
    $properties = [
      'type' => $content_type->id(),
      'entity_id' => $user->id(),
      'gid' => $group->id(),
    ];
    $contents = $this->contentStorage->loadByProperties($properties);

    if (!$contents && $create) {
      return $this->contentStorage->create($properties);
    }

    switch (count($contents)) {
      case 1:
        return current($contents);

      case 0:
        return;

      default:
        throw new \RuntimeException(
          'More than one group content entity found for user with ID %s and product group with ID %s',
          $user->id(),
          $group->id()
        );
    }
  }

  /**
   * Returns all product groups associated with the given group.
   *
   * @param \Drupal\group\Entity\GroupInterface $associated_group
   *   The group to return the product groups for.
   *
   * @return \Drupal\group\Entity\GroupInterface[]
   *   The associated product groups.
   */
  protected function getAssociatedProductGroups(
    GroupInterface $associated_group
  ) {
    $association_content_type = $this->getContentType(
      $associated_group->getGroupType(),
      ProductGroupGroupContentEnabler::PRODUCTS
    );
    if (!$association_content_type) {
      throw new \RuntimeException(
        'The %s group content enabler plugin is not installed on the %s group type.',
        ProductGroupGroupContentEnabler::PRODUCTS,
        $associated_group->bundle()
      );
    }

    // Get all group contents of product groups to the given associated group.
    $association_content_ids = $this->contentStorage
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', $association_content_type->id())
      ->condition('gid', $associated_group->id())
      ->execute();
    if (!$association_content_ids) {
      return [];
    }

    return array_map(
      function ($content) {
        return $content->getEntity();
      },
      $this->contentStorage->loadMultiple($association_content_ids)
    );
  }

  /**
   * Ensures that the given group content has the Group Customer group role.
   *
   * @param \Drupal\group\Entity\GroupContentInterface $content
   *   The group content entity that associates a user with a product group.
   *
   * @throws \RuntimeException
   *   If the given group content does not belong to a Products group.
   * @throws \RuntimeException
   *   If the given group content is not of Group Membership plugin.
   */
  protected function ensureRole(GroupContentInterface $content) {
    $bundle = $content->getGroup()->bundle();
    if ($bundle !== GroupBundle::PRODUCTS) {
      throw new \RuntimeException(sprintf(
        'Group content should belong to a group of %s type, %s given.',
        GroupBundle::PRODUCTS,
        $bundle
      ));
    }

    $plugin_id = $content->getContentPlugin()->getPluginId();
    if ($plugin_id !== GroupContentEnabler::MEMBERSHIP) {
      throw new \RuntimeException(sprintf(
        'Group content of %s plugin expected, %s given.',
        GroupContentEnabler::MEMBERSHIP,
        $plugin_id
      ));
    }

    $field_value = $content->get('group_roles')->getValue();
    // We do nothing if the role is referenced already.
    foreach ($field_value as $field_item_value) {
      if ($field_item_value['target_id'] === $this->customerRole->id()) {
        return;
      }
    }

    $field_value[] = ['target_id' => $this->customerRole->id()];
    $content->set('group_roles', $field_value);
    $this->contentStorage->save($content);
  }

  /**
   * Removes the Group Customer group role from the give group content.
   *
   * @param \Drupal\group\Entity\GroupContentInterface $content
   *   The group content entity that associates a user with a product group.
   *
   * @throws \RuntimeException
   *   If the given group content does not belong to a Products group.
   * @throws \RuntimeException
   *   If the given group content is not of Group Membership plugin.
   */
  protected function removeRole(GroupContentInterface $content) {
    $bundle = $content->getGroup()->bundle();
    if ($bundle !== GroupBundle::PRODUCTS) {
      throw new \RuntimeException(sprintf(
        'Group content should belong to a group of %s type, %s given.',
        GroupBundle::PRODUCTS,
        $bundle
      ));
    }

    $plugin_id = $content->getContentPlugin()->getPluginId();
    if ($plugin_id !== GroupContentEnabler::MEMBERSHIP) {
      throw new \RuntimeException(sprintf(
        'Group content of %s plugin expected, %s given.',
        GroupContentEnabler::MEMBERSHIP,
        $plugin_id
      ));
    }

    $changed = FALSE;
    $field_value = $content->get('group_roles')->getValue();
    foreach ($field_value as $delta => $field_item_value) {
      if ($field_item_value['target_id'] === $this->customerRole->id()) {
        $changed = TRUE;
        unset($field_value[$delta]);
      }
    }

    if (!$changed) {
      return;
    }

    // If the user still has other roles, they must have a non-managed
    // membership. We keep the membership.
    if (count($field_value)) {
      $content->set('group_roles', $field_value);
      $this->contentStorage->save($content);
    }
    // Otherwise, the membership is removed.
    else {
      $this->contentStorage->delete([$content]);
    }
  }

}
