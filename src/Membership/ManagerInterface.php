<?php

namespace Drupal\commerceg_product_group\Membership;

use Drupal\group\Entity\GroupInterface;
use Drupal\user\UserInterface;

/**
 * Manages product group memberships for associated group members.
 *
 * The Membership Manager implements automatic management of user memberships in
 * product groups on behalf of other associated groups. When a group is
 * associated with a product group, all group members are automatically added to
 * the product group with the Group Customer role so that they have access to
 * its products. When the association between the group and the product group is
 * removed, the memberships are removed.
 *
 * @I Add a clean-up method for auxiliary use
 *    type     : feature
 *    priority : low
 *    labels   : managed-memberships
 */
interface ManagerInterface {

  /**
   * Ensures that all associated group members are product group members.
   *
   * When a group is associated with a product group, all group members must
   * have a membership in the product group as well with the Group Customer
   * group role granted so that they have access to the group's products.
   *
   * @param \Drupal\group\Entity\GroupInterface $product_group
   *   The product group.
   * @param \Drupal\group\Entity\GroupInterface $associated_group
   *   The associated group.
   *
   * @see \Drupal\commerceg_product_group\Membership\ensureMembership()
   */
  public function ensureMembershipsForAssociatedGroup(
    GroupInterface $product_group,
    GroupInterface $associated_group
  );

  /**
   * Ensures product group memberships related to the user.
   *
   * When a user is added as a member to a group, the user must have memberships
   * with the Group Customer group role in all product groups associated with
   * the group.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   * @param \Drupal\group\Entity\GroupInterface $associated_group
   *   The associated group.
   *
   * @see \Drupal\commerceg_product_group\Membership\ensureMembership()
   */
  public function ensureMembershipsForUser(
    UserInterface $user,
    GroupInterface $associated_group
  );

  /**
   * Removes all product group memberships related to the associated group.
   *
   * When an association between a group and a product group is removed, all
   * group members must be disassociated from the product group so that they
   * do not have access to the group's products anymore - unless they have
   * been given access personally or through another group.
   *
   * @param \Drupal\group\Entity\GroupInterface $product_group
   *   The product group.
   * @param \Drupal\group\Entity\GroupInterface $associated_group
   *   The associated group.
   *
   * @see \Drupal\commerceg_product_group\Membership\removeMembership()
   */
  public function removeMembershipsForAssociatedGroup(
    GroupInterface $product_group,
    GroupInterface $associated_group
  );

  /**
   * Removes all product group memberships related to the user.
   *
   * When a membership of a user in a group is removed, all memberships
   * for the user in product groups that are associated with the associated
   * group must be removed - unless the user has been given access to the
   * product group personally or through another group.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   * @param \Drupal\group\Entity\GroupInterface $associated_group
   *   The associated group.
   *
   * @see \Drupal\commerceg_product_group\Membership\removeMembership()
   */
  public function removeMembershipsForUser(
    UserInterface $user,
    GroupInterface $associated_group
  );

  /**
   * Ensures that the user has a managed membership in the product group.
   *
   * - If the user does not have a product group membership, a membership is
   *   created and the Group Customer role is granted to the membership.
   * - If the user already has a membership without the Group Customer group
   *   role, the role is granted to the membership.
   * - If the user already has a membership with the Group Customer group role
   *   granted, no change is made.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to add the membership for.
   * @param \Drupal\group\Entity\GroupInterface $product_group
   *   The product group to add the membership in.
   */
  public function ensureMembership(
    UserInterface $user,
    GroupInterface $product_group
  );

  /**
   * Removes the managed membership for the user in the product group.
   *
   * The membership is removed on behalf of the given associated
   * group. Therefore, if the user has a personal membership in the product
   * group, or a managed membership on behalf of another group, we want those to
   * be maintained.
   *
   * With that in mind, the detailed behavior of this method is:
   *
   * - If the user has a product group membership, the user does not have the
   *   Customer role and is not member of any other associated group, the
   *   membership is removed.
   * - If the user has a product group membership, the user has the Customer
   *   role (or any other role) and is not member of any other associated group,
   *   the Group Customer role is removed and the membership is kept with the
   *   Customer role so that the user maintains their personal membership and
   *   access.
   * - If the user has a product group membership and is member of one or more
   *   other associated groups, no change is made.
   * - If the user has does not have a product group membership, no change is
   *   made.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to remove the membership for.
   * @param \Drupal\group\Entity\GroupInterface $product_group
   *   The product group to remove the membership from.
   * @param \Drupal\group\Entity\GroupInterface $associated_group
   *   The associated group on behalf of which the membership is removed.
   */
  public function removeMembership(
    UserInterface $user,
    GroupInterface $product_group,
    GroupInterface $associated_group
  );

}
