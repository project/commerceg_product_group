<?php

namespace Drupal\commerceg_product_group\Group;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerceg\Group\ContentLoader as ContentLoaderBase;
use Drupal\commerceg\MachineName\Plugin\GroupContentEnabler;
use Drupal\commerceg_product_group\MachineName\Bundle\Group as GroupBundle;

/**
 * Default implementation for the Commerce Product Group content loader service.
 */
class ContentLoader extends ContentLoaderBase implements
  ContentLoaderInterface {

  /**
   * {@inheritdoc}
   */
  public function loadForProduct(ProductInterface $product) {
    return $this->loadForEntityByPluginIdAndGroupTypeId(
      $product,
      GroupContentEnabler::PRODUCT . ':' . $product->bundle(),
      GroupBundle::PRODUCTS
    );
  }

}
