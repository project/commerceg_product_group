<?php

namespace Drupal\commerceg_product_group\Group;

use Drupal\commerce_product\Entity\ProductInterface;

/**
 * Facilitates loading of group content entities for Products groups.
 *
 * @see \Drupal\commerceg\Group\ContentLoaderInterface
 */
interface ContentLoaderInterface {

  /**
   * Loads the Products group content entities for the given product.
   *
   * It returns all content entities on any Products groups.
   *
   * @param \Drupal\commerce_product\Entity\ProductInterface $product
   *   The product.
   *
   * @return \Drupal\group\Entity\GroupContentInterface[]
   *   The group content entities for the product.
   *
   * @throws \Drupal\commerceg\Exception\RuntimeConfigurationException
   *   When the group content enabler plugin for the type (bundle) of the given
   *   product is not installed on Products group type.
   */
  public function loadForProduct(ProductInterface $product);

}
