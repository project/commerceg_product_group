<?php

namespace Drupal\commerceg_product_group\Configure;

use Drupal\commerceg\MachineName\Plugin\GroupContentEnabler;
use Drupal\commerceg_product_group\MachineName\Bundle\Group as GroupBundle;
use Drupal\commerceg_product_group\MachineName\ConfigEntity\GroupRole;
use Drupal\commerceg_product_group\MachineName\Plugin\GroupContentEnabler as ProductGroupGroupContentEnabler;

use Drupal\commerce_product\Entity\ProductTypeInterface;
use Drupal\group\Entity\GroupTypeInterface;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Default implementation of the Product Group Installer.
 *
 * Provides certain default configuration that makes sense for most
 * scenarios. Applications can override it to provide its own configuration
 * required.
 */
class Installer implements InstallerInterface {

  /**
   * The group content storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupContentStorageInterface
   */
  protected $contentStorage;

  /**
   * The group content type storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupContentTypeStorageInterface
   */
  protected $contentTypeStorage;

  /**
   * The group role storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupRoleStorageInterface
   */
  protected $groupRoleStorage;

  /**
   * The group type storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $groupTypeStorage;

  /**
   * Constructs a new Installer object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->contentStorage = $entity_type_manager->getStorage('group_content');
    $this->groupRoleStorage = $entity_type_manager->getStorage('group_role');
    $this->groupTypeStorage = $entity_type_manager->getStorage('group_type');

    $this->contentTypeStorage = $entity_type_manager
      ->getStorage('group_content_type');
  }

  /**
   * {@inheritdoc}
   */
  public function install() {
    $this->installGroupType();
    $this->installDefaultRoles();
  }

  /**
   * {@inheritdoc}
   */
  public function installGroupType() {
    // Do nothing if the group type already exists. This can happen if the
    // module is uninstalled and then installed again.
    if ($this->isGroupTypeInstalled()) {
      return;
    }

    $group_type = $this->groupTypeStorage->create([
      'id' => GroupBundle::PRODUCTS,
      'label' => 'Products',
      'description' => 'A group of products that can be used to restrict manager or customer access.',
    ]);
    $group_type->save();
  }

  /**
   * {@inheritdoc}
   */
  public function installDefaultRoles() {
    foreach ($this->getDefaultRoles() as $id => $role) {
      $this->installRole($id, $role['label']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function installProductType(ProductTypeInterface $product_type) {
    if ($this->isProductTypeInstalled($product_type)) {
      return;
    }

    $group_type = $this->groupTypeStorage->load(GroupBundle::PRODUCTS);
    $this->contentTypeStorage
      ->createFromPlugin(
        $group_type,
        GroupContentEnabler::PRODUCT . ':' . $product_type->id()
      )
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function uninstallProductType(ProductTypeInterface $product_type) {
    $content_type = $this->getProductTypeContentType($product_type);
    $this->contentTypeStorage->delete([$content_type]);
  }

  /**
   * {@inheritdoc}
   */
  public function isProductTypeInstalled(ProductTypeInterface $product_type) {
    return $this->getProductTypeContentType($product_type) ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function canUninstallProductType(ProductTypeInterface $product_type) {
    $content_type = $this->getProductTypeContentType($product_type);

    // We cannot uninstall the group content type if there is group content for
    // it.
    $count = $this->contentStorage
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', $content_type->id())
      ->count()
      ->execute();

    return $count ? FALSE : TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function installManagedMembershipRoles() {
    foreach ($this->getManagedMembershipRoles() as $id => $role) {
      $this->installRole($id, $role['label']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function installProductGroupContentType(
    GroupTypeInterface $group_type
  ) {
    if ($this->isProductGroupContentTypeInstalled($group_type)) {
      return;
    }

    $this->contentTypeStorage
      ->createFromPlugin(
        $group_type,
        ProductGroupGroupContentEnabler::PRODUCTS
      )
      ->save();
  }

  /**
   * Returns whether the Products group type is installed or not.
   *
   * @return bool
   *   TRUE if the Products group type exists, FALSE otherwise.
   */
  protected function isGroupTypeInstalled() {
    $group_type = $this->groupTypeStorage->load(GroupBundle::PRODUCTS);
    return $group_type ? TRUE : FALSE;
  }

  /**
   * Installs the role with the given ID and label.
   *
   * @param string $id
   *   The ID of the group role to create, including the group type prefix
   *   e.g. `commerceg_products-customer`.
   * @param string $label
   *   The label of the group role. It will be used only if the group role is
   *   created i.e. if the role already exists and it has a different label, it
   *   will not be corrected.
   */
  protected function installRole($id, $label) {
    if ($this->isRoleInstalled($id)) {
      return;
    }

    $role = $this->groupRoleStorage->create([
      'id' => $id,
      'label' => $label,
      'group_type' => GroupBundle::PRODUCTS,
    ]);
    $role->save();
  }

  /**
   * Returns whether the group role with the given ID exists or not.
   *
   * @param string $id
   *   The ID of the group role to check, including the group type prefix
   *   e.g. `commerceg_products-customer`.
   *
   * @return bool
   *   TRUE if the group role exists, FALSE otherwise.
   */
  protected function isRoleInstalled($id) {
    return $this->groupRoleStorage->load($id) ? TRUE : FALSE;
  }

  /**
   * Returns the default group roles required for general functionality.
   *
   * @return array
   *   An associative array keyed by the role ID and containing an associative
   *   array with further information about the role. Supported information is:
   *   - id: The ID (machine name) of the role.
   *   - label: The label (human-friendly name) of the role.
   */
  protected function getDefaultRoles() {
    return [
      GroupRole::CUSTOMER => [
        'id' => GroupRole::CUSTOMER,
        'label' => 'Customer',
      ],
      GroupRole::MANAGER => [
        'id' => GroupRole::MANAGER,
        'label' => 'Manager',
      ],
    ];
  }

  /**
   * Returns the content type for the given product type.
   *
   * @param Drupal\commerce_product\Entity\ProductTypeInterface $product_type
   *   The product type.
   *
   * @return \Drupal\group\Entity\GroupContentInterface|null
   *   The group content type, or NULL if the group content enabler plugin for
   *   the given product type on the Products group is not installed.
   */
  protected function getProductTypeContentType(
    ProductTypeInterface $product_type
  ) {
    $content_types = $this->contentTypeStorage->loadByContentPluginId(
      GroupContentEnabler::PRODUCT . ':' . $product_type->id()
    );
    foreach ($content_types as $content_type) {
      if ($content_type->getGroupType()->id() !== GroupBundle::PRODUCTS) {
        continue;
      }

      return $content_type;
    }
  }

  /**
   * Returns the group roles required for the managed memberships functionality.
   *
   * @return array
   *   An associative array keyed by the role ID and containing an associative
   *   array with further information about the role. Supported information is:
   *   - id: The ID (machine name) of the role.
   *   - label: The label (human-friendly name) of the role.
   */
  protected function getManagedMembershipRoles() {
    return [
      GroupRole::GROUP_CUSTOMER => [
        'id' => GroupRole::GROUP_CUSTOMER,
        'label' => 'Group customer',
      ],
    ];
  }

  /**
   * Returns whether the content type for the given group type exists.
   *
   * More specifically, it checks for the group content type that associates
   * product groups with groups of the given type.
   *
   * @param \Drupal\group\Entity\GroupTypeInterface $group_type
   *   The group type for which to check whether the group content type exists.
   */
  protected function isProductGroupContentTypeInstalled(
    GroupTypeInterface $group_type
  ) {
    return $this->getProductGroupContentType($group_type) ? TRUE : FALSE;
  }

  /**
   * Returns the content type for the given type.
   *
   * More specifically, it returns the group content type that associates
   * product groups with groups of the given type.
   *
   * @param \Drupal\group\Entity\GroupTypeInterface $group_type
   *   The group type for which to return the group content type.
   *
   * @return \Drupal\group\Entity\GroupContentInterface|null
   *   The group content type, or NULL if the product group is not installed on
   *   the given group type.
   */
  protected function getProductGroupContentType(
    GroupTypeInterface $group_type
  ) {
    $content_types = $this->contentTypeStorage->loadByContentPluginId(
      ProductGroupGroupContentEnabler::PRODUCTS
    );
    foreach ($content_types as $content_type) {
      if ($content_type->getGroupType()->id() !== $group_type->id()) {
        continue;
      }

      return $content_type;
    }
  }

}
