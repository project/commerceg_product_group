<?php

namespace Drupal\commerceg_product_group\Configure\Controller;

use Drupal\commerceg\MachineName\Plugin\GroupContentEnabler;
use Drupal\commerceg_product_group\MachineName\Bundle\Group as GroupBundle;

use Drupal\commerce_product\ProductTypeListBuilder as ListBuilderBase;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;

/**
 * Builds a list of product types in relation to Product Group functionality.
 */
class ProductTypeListBuilder extends ListBuilderBase {

  /**
   * The group content type storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupContentTypeStorageInterface
   */
  protected $contentTypeStorage;

  /**
   * The group type storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $groupTypeStorage;

  /**
   * The list of already installed product types.
   *
   * Keyed by the product type ID, holds the ID of the group content type
   * enabling the integration.
   *
   * @var array
   */
  protected $installedTypes;

  /**
   * Constructs a new ProductTypeListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($entity_type, $storage, $entity_type_manager);

    $this->contentTypeStorage = $entity_type_manager
      ->getStorage('group_content_type');
    $this->groupTypeStorage = $entity_type_manager
      ->getStorage('group_type');

    $this->buildInstalledTypes();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = $this->t('Product type');
    $header['type'] = $this->t('ID');
    $header['product_variation_type'] = $this->t('Product variation type');
    $header['description'] = $this->t('Description');
    $header['status'] = $this->t('Integration status');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['name']['data'] = [
      '#type' => 'link',
      '#title' => $entity->label(),
      '#url' => $entity->toUrl('edit-form'),
    ];
    $row['type'] = $entity->id();

    $variation_type = $this->variationTypeStorage
      ->load($entity->getVariationTypeId());
    if (empty($variation_type)) {
      $row['product_variation_type'] = $this->t('N/A');
    }
    else {
      $row['product_variation_type']['data'] = [
        '#type' => 'link',
        '#title' => $variation_type->label(),
        '#url' => $variation_type->toUrl('edit-form'),
      ];
    }

    $row['description'] = $entity->getDescription();

    if (in_array($entity->id(), array_keys($this->installedTypes))) {
      // @I Include information when the configuration is not the default
      //   type     : improvement
      //   priority : normal
      //   labels   : config
      $row['status'] = $this->t(
        '<em>Installed</em>: products of this type can be added to product
         groups.'
      );
    }
    else {
      $row['status'] = $this->t(
        '<em>Available</em>: products of this type cannot be added to product
         groups until integration is enabled.'
      );
    }

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getOperations(EntityInterface $entity) {
    $installed = in_array($entity->id(), array_keys($this->installedTypes));

    if (!$installed) {
      return [
        'install' => [
          'title' => $this->t('Install'),
          'url' => Url::fromRoute(
            'commerceg_product_group.config.install_product_type',
            ['commerce_product_type' => $entity->id()]
          ),
        ],
      ];
    }

    // If the product type is already installed, provide links to configure,
    // reset configuration to the defaults, and uninstall.
    return [
      // @I Redirect back to the product type configuration page
      //   type     : improvement
      //   priority : low
      //   labels   : config
      'configure' => [
        'title' => $this->t('Configure'),
        'url' => Url::fromRoute(
          'entity.group_content_type.edit_form',
          ['group_content_type' => $this->installedTypes[$entity->id()]]
        ),
      ],
      // @I Do not link to reset configuration when it is the default
      //   type     : improvement
      //   priority : low
      //   labels   : config, product-group
      'reset' => [
        'title' => $this->t('Reset configuration'),
        'url' => Url::fromRoute(
          'commerceg_product_group.config.reset_product_type',
          ['commerce_product_type' => $entity->id()]
        ),
      ],
      'uninstall' => [
        'title' => $this->t('Uninstall'),
        'url' => Url::fromRoute(
          'entity.group_content_type.delete_form',
          ['group_content_type' => $this->installedTypes[$entity->id()]]
        ),
      ],
    ];
  }

  /**
   * Builds the list of already installed product types.
   */
  protected function buildInstalledTypes() {
    $group_type = $this->groupTypeStorage->load(GroupBundle::PRODUCTS);
    $content_types = $this->contentTypeStorage->loadByGroupType($group_type);
    $base_plugin_id_length = strlen(GroupContentEnabler::PRODUCT) + 1;

    $this->installedTypes = [];
    foreach ($content_types as $content_type) {
      $plugin_id = $content_type->getContentPluginId();
      if (strpos($plugin_id, GroupContentEnabler::PRODUCT . ':') === FALSE) {
        continue;
      }

      $product_type_id = substr($plugin_id, $base_plugin_id_length);
      $this->installedTypes[$product_type_id] = $content_type->id();
    }
  }

}
