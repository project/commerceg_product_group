<?php

namespace Drupal\commerceg_product_group\Configure;

use Drupal\commerce_product\Entity\ProductTypeInterface;
use Drupal\group\Entity\GroupTypeInterface;

/**
 * Installs configuration as expected by Product Group functionality.
 *
 * Commerce Product Group heavily relies on Commerce Group to provide base
 * integration between Commerce and Group. It then builds on top of certain
 * configuration e.g. a Products group type with product type content enablers
 * installed on it so that it can offer the required functionality.
 *
 * The installer and the configurator services make it easy to prepare such
 * configuration.
 *
 * Note that the expected behavior of the Installer is to install expected
 * configuration if it doesn't exist, but to not throw exceptions if it is
 * already installed. That way, it can be safely run again as additions are
 * being made without errors on previously installed configuration. Uninstalling
 * is different though; exceptions should be thrown when trying to uninstall
 * something that is not installed.
 *
 * @I Add `diff` method that reports deviations from expected configuration
 *   type     : improvement
 *   priority : normal
 *   labels   : 1.0.0, config
 */
interface InstallerInterface {

  /**
   * Installs all Product group-related configuration.
   *
   * It includes:
   *   - Installing Products group type.
   */
  public function install();

  /**
   * Installs the Products group type, if it doesn't exist.
   */
  public function installGroupType();

  /**
   * Installs the default product group roles.
   *
   * The default group roles are the ones required for general functionality
   * provided by the module.
   */
  public function installDefaultRoles();

  /**
   * Enables Product Group integration for the given product type.
   *
   * It includes:
   *   - Installing the corresponding group content enabler plugin for the
   *     Products group type.
   *
   * @param Drupal\commerce_product\Entity\ProductTypeInterface $product_type
   *   The product type to install.
   */
  public function installProductType(ProductTypeInterface $product_type);

  /**
   * Disables Product Group integration for the given product type.
   *
   * @param Drupal\commerce_product\Entity\ProductTypeInterface $product_type
   *   The product type to uninstall.
   */
  public function uninstallProductType(ProductTypeInterface $product_type);

  /**
   * Returns if Product Group integration for a product type is enabled.
   *
   * @param Drupal\commerce_product\Entity\ProductTypeInterface $product_type
   *   The product type to check.
   *
   * @return bool
   *   TRUE if the product type is installed i.e. integration enabled, FALSE
   *   otherwise.
   *
   * @see \Drupal\commerceg_product_group\Configure\InstallerInterface::installProductType()
   */
  public function isProductTypeInstalled(ProductTypeInterface $product_type);

  /**
   * Returns if Product Group integration for a product type can be disabled.
   *
   * @param Drupal\commerce_product\Entity\ProductTypeInterface $product_type
   *   The product type to check.
   *
   * @return bool
   *   TRUE if the product type can be uninstalled i.e. disable integration,
   *   FALSE otherwise.
   */
  public function canUninstallProductType(ProductTypeInterface $product_type);

  /**
   * Installs the group roles required for the managed memberships feature.
   */
  public function installManagedMembershipRoles();

  /**
   * Installs the content type for the given group type.
   *
   * The content type installed is the one that connects product groups with
   * groups of the given type, using the `commerceg_products` group content
   * enabler plugin.
   *
   * @param \Drupal\group\Entity\GroupTypeInterface $group_type
   *   The group type for which to install the group content type.
   */
  public function installProductGroupContentType(
    GroupTypeInterface $group_type
  );

}
