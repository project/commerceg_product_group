<?php

namespace Drupal\commerceg_product_group\Configure;

use Drupal\commerceg\MachineName\Plugin\GroupContentEnabler;
use Drupal\commerceg_product_group\MachineName\Bundle\Group as GroupBundle;
use Drupal\commerceg_product_group\MachineName\ConfigEntity\GroupRole;
use Drupal\commerceg_product_group\MachineName\Plugin\GroupContentEnabler as ProductGroupGroupContentEnabler;

use Drupal\commerce_product\Entity\ProductTypeInterface;
use Drupal\group\Entity\GroupTypeInterface;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Default implementation of the Product Group configurator.
 *
 * Provides certain default configuration that makes sense for most
 * scenarios. Applications can override it to provide its own configuration
 * required.
 */
class Configurator implements ConfiguratorInterface {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The group content type storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupContentTypeStorageInterface
   */
  protected $contentTypeStorage;

  /**
   * The group role storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupRoleStorageInterface
   */
  protected $groupRoleStorage;

  /**
   * The group type storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $groupTypeStorage;

  /**
   * The product type storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $productTypeStorage;

  /**
   * Constructs a new Configurator object.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityFieldManager = $entity_field_manager;
    $this->groupRoleStorage = $entity_type_manager->getStorage('group_role');
    $this->groupTypeStorage = $entity_type_manager->getStorage('group_type');

    $this->contentTypeStorage = $entity_type_manager
      ->getStorage('group_content_type');
    $this->productTypeStorage = $entity_type_manager
      ->getStorage('commerce_product_type');
  }

  /**
   * {@inheritdoc}
   */
  public function configure() {
    $this->configureGroupType();
    $this->configureDefaultRoles();
  }

  /**
   * {@inheritdoc}
   */
  public function configureGroupType() {
    $group_type = $this->groupTypeStorage->load(GroupBundle::PRODUCTS);
    if (!$group_type) {
      throw new \RuntimeException(
        'Cannot configure a group type that does not exist.'
      );
    }

    $group_type->set('creator_membership', FALSE);
    $group_type->set('creator_wizard', FALSE);
    $group_type->save();

    $this->configureGroupTypeTitleFieldLabel();
  }

  /**
   * {@inheritdoc}
   */
  public function configureDefaultRoles() {
    $this->configureRoles(
      $this->getDefaultRoles(),
      $this->getDefaultRolePermissions()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function configureManagedMembershipRoles() {
    $this->configureRoles(
      $this->getManagedMembershipRoles(),
      $this->getManagedMembershipRolePermissions()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function configureProductType(ProductTypeInterface $product_type) {
    $content_types = $this->contentTypeStorage->loadByContentPluginId(
      GroupContentEnabler::PRODUCT . ':' . $product_type->id()
    );
    foreach ($content_types as $content_type) {
      if ($content_type->getGroupType()->id() !== GroupBundle::PRODUCTS) {
        continue;
      }

      $content_type->updateContentPlugin([
        // By default products can belong to multiple product groups.
        'group_cardinality' => 0,
        // We assume that it only makes sense that a product has only one group
        // content on the same product group. If that's not the case a different
        // group content enabler plugin is likely to be used for the specific
        // use case.
        'entity_cardinality' => 1,
        // We assume that the group content type does not have additional
        // fields; it is therefore not necessary to use the wizard.
        'use_creation_wizard' => 0,
      ]);
      $content_type->save();

      return;
    }

    throw new \RuntimeException(
      'Cannot configure a plugin that is not installed.'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function configureProductGroupContentType(
    GroupTypeInterface $group_type
  ) {
    $content_types = $this->contentTypeStorage->loadByContentPluginId(
      ProductGroupGroupContentEnabler::PRODUCTS
    );
    foreach ($content_types as $content_type) {
      if ($content_type->getGroupType()->id() !== $group_type->id()) {
        continue;
      }

      $content_type->updateContentPlugin([
        // By default product groups can added to multiple associated groups.
        'group_cardinality' => 0,
        // We assume that it only makes sense that a product group has only one
        // group content on the same associated group. If that's not the case a
        // different group content enabler plugin is likely to be used for the
        // specific use case.
        'entity_cardinality' => 1,
        // We assume that the group content type does not have additional
        // fields; it is therefore not necessary to use the wizard.
        'use_creation_wizard' => 0,
      ]);
      $content_type->save();

      return;
    }

    throw new \RuntimeException(
      'Cannot configure a plugin that is not installed.'
    );
  }

  /**
   * Configures the title field label for the Products group type.
   *
   * `Name` makes more sense than `Title` for product groups.
   */
  protected function configureGroupTypeTitleFieldLabel() {
    $title_field = $this->entityFieldManager
      ->getFieldDefinitions('group', GroupBundle::PRODUCTS)
      ['label'];

    $title_field
      ->getConfig(GroupBundle::PRODUCTS)
      ->setLabel('Name')
      ->save();
  }

  /**
   * Resets the given role permissions to the given values.
   *
   * @param \Drupal\group\Entity\GroupRoleInterface[] $roles
   *   The group roles.
   * @param array $permissions
   *   An associative array keyed by the group role ID and containing an
   *   associative array with the permissions. The permissions array is keyed by
   *   the permission ID (machine name) and containing the value to set (TRUE or
   *   FALSE to set it as the new value, or NULL to keep the current value
   *   unchanged).
   */
  protected function configureRoles(array $roles, array $permissions) {
    foreach ($roles as $role) {
      if (!isset($permissions[$role->id()])) {
        continue;
      }

      $role->changePermissions($permissions[$role->id()]);
      $this->groupRoleStorage->save($role);
    }
  }

  /**
   * Returns the default role entities required by this module.
   *
   * @return \Drupal\group\Entity\GroupRoleInterface[]
   *   The default group roles.
   */
  protected function getDefaultRoles() {
    return $this->groupRoleStorage->loadMultiple(
      array_keys($this->getDefaultRolesInfo())
    );
  }

  /**
   * Returns the role entities required by the managed membership feature.
   *
   * @return \Drupal\group\Entity\GroupRoleInterface[]
   *   The managed membership group roles.
   */
  protected function getManagedMembershipRoles() {
    return $this->groupRoleStorage->loadMultiple(
      array_keys($this->getManagedMembershipRolesInfo())
    );
  }

  /**
   * Returns the default group roles required for general functionality.
   *
   * @return array
   *   An associative array keyed by the role ID and containing an associative
   *   array with further information about the role. Supported information is:
   *   - id: The ID (machine name) of the role.
   *   - label: The label (human-friendly name) of the role.
   */
  protected function getDefaultRolesInfo() {
    return [
      GroupRole::ANONYMOUS => [
        'id' => GroupRole::ANONYMOUS,
        'label' => 'Anonymous',
      ],
      GroupRole::CUSTOMER => [
        'id' => GroupRole::CUSTOMER,
        'label' => 'Customer',
      ],
      GroupRole::MANAGER => [
        'id' => GroupRole::MANAGER,
        'label' => 'Manager',
      ],
      GroupRole::MEMBER => [
        'id' => GroupRole::MEMBER,
        'label' => 'Member',
      ],
      GroupRole::OUTSIDER => [
        'id' => GroupRole::OUTSIDER,
        'label' => 'Outsider',
      ],
    ];
  }

  /**
   * Returns the group roles required for the managed memberships functionality.
   *
   * @return array
   *   An associative array keyed by the role ID and containing an associative
   *   array with further information about the role. Supported information is:
   *   - id: The ID (machine name) of the role.
   *   - label: The label (human-friendly name) of the role.
   */
  protected function getManagedMembershipRolesInfo() {
    return [
      GroupRole::GROUP_CUSTOMER => [
        'id' => GroupRole::GROUP_CUSTOMER,
        'label' => 'Group customer',
      ],
    ];
  }

  /**
   * Returns the permissions for the default group roles.
   *
   * @return array
   *   An associative array keyed by the group role ID and containing an
   *   associative array with the permissions. The permissions array is keyed by
   *   the permission ID (machine name) and containing the value to set (TRUE or
   *   FALSE to set it as the new value, or NULL to keep the current value
   *   unchanged).
   */
  protected function getDefaultRolePermissions() {
    $permissions = [
      GroupRole::ANONYMOUS => [
        // Group permissions.
        'access content overview' => FALSE,
        'administer group' => FALSE,
        'delete group' => FALSE,
        'edit group' => FALSE,
        'view group' => FALSE,
        // Group membership permissions.
        'administer members' => FALSE,
        'update own group_membership content' => FALSE,
        'join group' => FALSE,
        'leave group' => FALSE,
        'view group_membership content' => FALSE,
        // Product permissions.
        'access commerceg_product_group overview' => FALSE,
        'access commerceg_product overview' => FALSE,
        'create commerceg_product: entity' => FALSE,
        'delete any commerceg_product: entity' => FALSE,
        'delete own commerceg_product: entity' => FALSE,
        'update any commerceg_product: entity' => FALSE,
        'update own commerceg_product: entity' => FALSE,
        'view commerceg_product: entity' => FALSE,
        'view any unpublished commerceg_product: entity' => FALSE,
        'create commerceg_product: content' => FALSE,
        'delete any commerceg_product: content' => FALSE,
        'delete own commerceg_product: content' => FALSE,
        'update any commerceg_product: content' => FALSE,
        'update own commerceg_product: content' => FALSE,
        'view commerceg_product: content' => FALSE,
      ],
      GroupRole::OUTSIDER => [
        // Group permissions.
        'access content overview' => FALSE,
        'administer group' => FALSE,
        'delete group' => FALSE,
        'edit group' => FALSE,
        'view group' => FALSE,
        // Group membership permissions.
        'administer members' => FALSE,
        'update own group_membership content' => FALSE,
        'join group' => FALSE,
        'leave group' => FALSE,
        'view group_membership content' => FALSE,
        // Product permissions.
        'access commerceg_product_group overview' => FALSE,
        'access commerceg_product overview' => FALSE,
        'create commerceg_product: entity' => FALSE,
        'delete any commerceg_product: entity' => FALSE,
        'delete own commerceg_product: entity' => FALSE,
        'update any commerceg_product: entity' => FALSE,
        'update own commerceg_product: entity' => FALSE,
        'view commerceg_product: entity' => FALSE,
        'view any unpublished commerceg_product: entity' => FALSE,
        'create commerceg_product: content' => FALSE,
        'delete any commerceg_product: content' => FALSE,
        'delete own commerceg_product: content' => FALSE,
        'update any commerceg_product: content' => FALSE,
        'update own commerceg_product: content' => FALSE,
        'view commerceg_product: content' => FALSE,
      ],
      GroupRole::MEMBER => [
        // Group permissions.
        'access content overview' => FALSE,
        'administer group' => FALSE,
        'delete group' => FALSE,
        'edit group' => FALSE,
        'view group' => FALSE,
        // Group membership permissions.
        'administer members' => FALSE,
        'update own group_membership content' => FALSE,
        'join group' => FALSE,
        'leave group' => FALSE,
        'view group_membership content' => FALSE,
        // Product permissions.
        'access commerceg_product_group overview' => FALSE,
        'access commerceg_product overview' => FALSE,
        'create commerceg_product: entity' => FALSE,
        'delete any commerceg_product: entity' => FALSE,
        'delete own commerceg_product: entity' => FALSE,
        'update any commerceg_product: entity' => FALSE,
        'update own commerceg_product: entity' => FALSE,
        'view commerceg_product: entity' => FALSE,
        'view any unpublished commerceg_product: entity' => FALSE,
        'create commerceg_product: content' => FALSE,
        'delete any commerceg_product: content' => FALSE,
        'delete own commerceg_product: content' => FALSE,
        'update any commerceg_product: content' => FALSE,
        'update own commerceg_product: content' => FALSE,
        'view commerceg_product: content' => FALSE,
      ],
      GroupRole::MANAGER => [
        // Group permissions.
        'access content overview' => FALSE,
        'administer group' => FALSE,
        'delete group' => TRUE,
        'edit group' => TRUE,
        'view group' => TRUE,
        // Group membership permissions.
        'administer members' => TRUE,
        'update own group_membership content' => NULL,
        'join group' => NULL,
        'leave group' => NULL,
        'view group_membership content' => NULL,
        // Product permissions.
        'access commerceg_product_group overview' => FALSE,
        'access commerceg_product overview' => TRUE,
        'create commerceg_product: entity' => TRUE,
        'delete any commerceg_product: entity' => TRUE,
        'delete own commerceg_product: entity' => NULL,
        'update any commerceg_product: entity' => TRUE,
        'update own commerceg_product: entity' => NULL,
        'view commerceg_product: entity' => TRUE,
        'view any unpublished commerceg_product: entity' => TRUE,
        'create commerceg_product: content' => TRUE,
        'delete any commerceg_product: content' => TRUE,
        'delete own commerceg_product: content' => NULL,
        'update any commerceg_product: content' => TRUE,
        'update own commerceg_product: content' => NULL,
        'view commerceg_product: content' => TRUE,
      ],
      GroupRole::CUSTOMER => [
        // Group permissions.
        'access content overview' => FALSE,
        'administer group' => FALSE,
        'delete group' => FALSE,
        'edit group' => FALSE,
        'view group' => FALSE,
        // Group membership permissions.
        'administer members' => FALSE,
        'update own group_membership content' => FALSE,
        'join group' => FALSE,
        'leave group' => FALSE,
        'view group_membership content' => FALSE,
        // Product permissions.
        'access commerceg_product_group overview' => FALSE,
        'access commerceg_product overview' => FALSE,
        'create commerceg_product: entity' => FALSE,
        'delete any commerceg_product: entity' => FALSE,
        'delete own commerceg_product: entity' => FALSE,
        'update any commerceg_product: entity' => FALSE,
        'update own commerceg_product: entity' => FALSE,
        'view commerceg_product: entity' => TRUE,
        'view any unpublished commerceg_product: entity' => FALSE,
        'create commerceg_product: content' => FALSE,
        'delete any commerceg_product: content' => FALSE,
        'delete own commerceg_product: content' => FALSE,
        'update any commerceg_product: content' => FALSE,
        'update own commerceg_product: content' => FALSE,
        'view commerceg_product: content' => FALSE,
      ],
    ];

    return $this->buildPermissions($permissions);
  }

  /**
   * Returns the permissions for the managed membership group roles.
   *
   * @return array
   *   An associative array keyed by the group role ID and containing an
   *   associative array with the permissions. The permissions array is keyed by
   *   the permission ID (machine name) and containing the value to set (TRUE or
   *   FALSE to set it as the new value, or NULL to keep the current value
   *   unchanged).
   */
  protected function getManagedMembershipRolePermissions() {
    $permissions = [
      GroupRole::GROUP_CUSTOMER => [
        // Group permissions.
        'access content overview' => FALSE,
        'administer group' => FALSE,
        'delete group' => FALSE,
        'edit group' => FALSE,
        'view group' => FALSE,
        // Group membership permissions.
        'administer members' => FALSE,
        'update own group_membership content' => FALSE,
        'join group' => FALSE,
        'leave group' => FALSE,
        'view group_membership content' => FALSE,
        // Product permissions.
        'access commerceg_product_group overview' => FALSE,
        'access commerceg_product overview' => FALSE,
        'create commerceg_product: entity' => FALSE,
        'delete any commerceg_product: entity' => FALSE,
        'delete own commerceg_product: entity' => FALSE,
        'update any commerceg_product: entity' => FALSE,
        'update own commerceg_product: entity' => FALSE,
        'view commerceg_product: entity' => TRUE,
        'view any unpublished commerceg_product: entity' => FALSE,
        'create commerceg_product: content' => FALSE,
        'delete any commerceg_product: content' => FALSE,
        'delete own commerceg_product: content' => FALSE,
        'update any commerceg_product: content' => FALSE,
        'update own commerceg_product: content' => FALSE,
        'view commerceg_product: content' => FALSE,
      ],
    ];

    return $this->buildPermissions($permissions);
  }

  /**
   * Replaces base permissions with all derivative ones per product type.
   *
   * To make it easy to define the default permissions for all product types at
   * once, we define them with `commerceg_product:` as the plugin ID. We need to
   * convert them to the real ones that make use of the product type
   * e.g. `commerceg_product:default`.
   *
   * @param array $permissions
   *   An associative array keyed by the group role ID and containing an
   *   associative array with the permissions. The permissions array is keyed by
   *   the permission ID (machine name) and containing the value to set (TRUE or
   *   FALSE to set it as the new value, or NULL to keep the current value
   *   unchanged).
   *
   * @return array
   *   An associative array with the same structure as the given permissions,
   *   but with the placeholder product-related permissions removed and the
   *   corresponding permissions per product type added with the same values.
   */
  protected function buildPermissions(array $permissions) {
    $product_type_ids = $this->productTypeStorage
      ->getQuery()
      ->accessCheck(FALSE)
      ->execute();

    foreach ($permissions as &$role_permissions) {
      foreach ($role_permissions as $role_permission => $value) {
        if (strpos($role_permission, 'commerceg_product:') === FALSE) {
          continue;
        }

        foreach ($product_type_ids as $product_type_id) {
          $role_permissions[str_replace('commerceg_product:', "commerceg_product:{$product_type_id}", $role_permission)] = $value;
        }

        unset($role_permissions[$role_permission]);
      }
    }

    return $permissions;
  }

}
