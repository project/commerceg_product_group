<?php

namespace Drupal\commerceg_product_group\Configure;

use Drupal\commerce_product\Entity\ProductTypeInterface;
use Drupal\group\Entity\GroupTypeInterface;

/**
 * Configures the application as expected by Product Group functionality.
 *
 * Commerce Product Group heavily relies on Commerce Group to provide base
 * integration between Commerce and Group. It then builds on top of certain
 * configuration e.g. a Products group type with product type content enablers
 * installed on it so that it can offer the required functionality.
 *
 * The installer and the configurator services make it easy to prepare such
 * configuration.
 *
 * Note that the expected behavior of the Configurator is to throw exceptions
 * when configuration entities or settings being configured do not exist. This
 * enforces to first run the Installer before configuring.
 *
 * @I Add `diff` method that reports deviations from expected configuration
 *   type     : improvement
 *   priority : normal
 *   labels   : 1.0.0, config
 */
interface ConfiguratorInterface {

  /**
   * Configures all Product group-related settings.
   *
   * It includes:
   *   - Configures the Products group type.
   */
  public function configure();

  /**
   * Configures the Products group type.
   *
   * @throws \RuntimeException
   *   If the group type is not installed.
   */
  public function configureGroupType();

  /**
   * Configures the default group roles.
   *
   * It includes:
   *   - Resets the permissions for the default group roles to the default
   *     values. Permissions that are not related to functionality provided by
   *     module will be left unchanged.
   */
  public function configureDefaultRoles();

  /**
   * Configures the managed membership group roles.
   *
   * It includes:
   *   - Resets the permissions for the managed membership group roles to the
   *     default values. Permissions that are not related to functionality
   *     provided by this module will be left unchanged.
   */
  public function configureManagedMembershipRoles();

  /**
   * Configures the Product Group integration for the given product type.
   *
   * It includes:
   *   - Configures the corresponding group content enabler plugin.
   *
   * @param Drupal\commerce_product\Entity\ProductTypeInterface $product_type
   *   The product type to configure.
   *
   * @throws \RuntimeException
   *   If the Product Group integration for the given product type is not
   *   enabled.
   */
  public function configureProductType(ProductTypeInterface $product_type);

  /**
   * Configures the content type for the given group type.
   *
   * The content type configured is the one that connects product groups with
   * groups of the given type, using the `commerceg_products` group content
   * enabler plugin.
   *
   * @param \Drupal\group\Entity\GroupTypeInterface $group_type
   *   The group type for which to configure the group content type.
   *
   * @throws \RuntimeException
   *   If the group content type is not installed for the given group type.
   */
  public function configureProductGroupContentType(
    GroupTypeInterface $group_type
  );

}
