<?php

namespace Drupal\commerceg_product_group\Configure\Form;

use Drupal\commerceg\Form\FormBase;
use Drupal\commerceg_product_group\Configure\Configurator;
use Drupal\commerceg_product_group\Configure\Installer;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class InstallProductType extends FormBase {

  protected $configurator;

  protected $installer;

  protected $productType;

  public function __construct(
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $logger_factory,
    MessengerInterface $messenger,
    RedirectDestinationInterface $redirect_destination,
    RequestStack $request_stack,
    RouteMatchInterface $route_match,
    TranslationInterface $string_translation,
    Configurator $configurator,
    Installer $installer
  ) {
    parent::__construct(
      $config_factory,
      $logger_factory,
      $messenger,
      $redirect_destination,
      $request_stack,
      $route_match,
      $string_translation
    );

    $this->configurator = $configurator;
    $this->installer = $installer;

    $this->productType = $this->routeMatch
      ->getParameter('commerce_product_type');
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('redirect.destination'),
      $container->get('request_stack'),
      $container->get('current_route_match'),
      $container->get('string_translation'),
      $container->get('commerceg_product_group.configurator'),
      $container->get('commerceg_product_group.installer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerceg_product_group_install_product_type';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($this->installer->isProductTypeInstalled($this->productType)) {
      return $this->buildInstalledForm($form, $form_state);
    }

    $form['help'] = [
      '#markup' => '<p>' . $this->t(
        'Installing the %label product type on product groups will allow
         products of this type to be added to such groups. Are you sure?',
        ['%label' => $this->productType->label()]
      ) . '</p>',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Install'),
    ];
    $form['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromRoute('commerceg_product_group.config.product_types'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->installer->installProductType($this->productType);
    $this->configurator->configureProductType($this->productType);

    $this->messenger->addStatus(
      $this->t(
        'The %label product type has been successfully installed on product
         groups',
        ['%label' => $this->productType->label()]
      )
    );

    $form_state->setRedirect('commerceg_product_group.config.product_types');
  }

  /**
   * Builds the form for the case that the product type is installed.
   *
   * We cannot install an already installed product type.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The form render array.
   */
  protected function buildInstalledForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form['help'] = [
      '#markup' => '<p>' . $this->t(
        'The %label product type is already installed on product groups.',
        ['%label' => $this->productType->label()]
      ) . '</p>',
    ];
    $form['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Go back'),
      '#url' => Url::fromRoute('commerceg_product_group.config.product_types'),
    ];

    return $form;
  }

}
