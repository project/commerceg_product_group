<?php

namespace Drupal\commerceg_product_group\Configure\Form;

use Drupal\commerceg\Form\ConfigFormBase;
use Drupal\commerceg_product_group\Configure\ConfiguratorInterface;
use Drupal\commerceg_product_group\Configure\InstallerInterface;
use Drupal\commerceg_product_group\MachineName\Bundle\Group as GroupBundle;

use Drupal\commerce\EntityHelper;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\TranslationInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Configure shopping context-related settings.
 */
class Settings extends ConfigFormBase {

  /**
   * The Product Group configurator.
   *
   * @var \Drupal\commerceg_product_group\Configure\ConfiguratorInterface
   */
  protected $configurator;

  /**
   * The Product Group installer.
   *
   * @var \Drupal\commerceg_product_group\Configure\InstallerInterface
   */
  protected $installer;

  /**
   * The group type storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $groupTypeStorage;

  /**
   * Constructs a new Settings object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerceg_product_group\Configure\ConfiguratorInterface $configurator
   *   The Product Group organization configurator.
   * @param \Drupal\commerceg_product_group\Configure\InstallerInterface $installer
   *   The Product Group organization installer.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $logger_factory,
    MessengerInterface $messenger,
    RedirectDestinationInterface $redirect_destination,
    RequestStack $request_stack,
    RouteMatchInterface $route_match,
    TranslationInterface $string_translation,
    EntityTypeManagerInterface $entity_type_manager,
    ConfiguratorInterface $configurator,
    InstallerInterface $installer
  ) {
    parent::__construct(
      $config_factory,
      $logger_factory,
      $messenger,
      $redirect_destination,
      $request_stack,
      $route_match,
      $string_translation
    );

    $this->configurator = $configurator;
    $this->installer = $installer;
    $this->groupTypeStorage = $entity_type_manager->getStorage('group_type');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('redirect.destination'),
      $container->get('request_stack'),
      $container->get('current_route_match'),
      $container->get('string_translation'),
      $container->get('entity_type.manager'),
      $container->get('commerceg_product_group.configurator'),
      $container->get('commerceg_product_group.installer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerceg_product_group_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'commerceg_product_group.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('commerceg_product_group.settings');

    $form['#tree'] = TRUE;
    $form['managed_memberships']['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable product group membership management'),
      '#description' => $this->t(
        'When Product Group membership management is enabled, users members of
         groups associated with Product Groups (such as organizations or
         customer groups) will be automatically added to associated Product
         Groups with the Group Customer group role so that they have access to
         their products. When disabled, associating a Product Group with another
         group will not have any immediate effect and the group members will not
         have access to the products unless they are manually added to the
         product group.'
      ),
      '#default_value' => $config->get('managed_memberships.status'),
    ];

    $group_types = $this->groupTypeStorage->loadMultiple();
    $group_types = array_filter(
      $group_types,
      function ($group_type) {
        return $group_type->id() === GroupBundle::PRODUCTS ? FALSE : TRUE;
      }
    );
    $form['managed_memberships']['group_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Managed group types'),
      '#description' => $this->t(
        'Groups of the selected types will have their member users automatically
         added to their associated product groups, and removed when such
         associations are cancelled.'
      ),
      '#options' => EntityHelper::extractLabels($group_types),
      '#default_value' => $config->get('managed_memberships.group_types') ?? [],
      '#states' => [
        'visible' => [
          ':input[data-drupal-selector="edit-managed-memberships-status"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (!$values['managed_memberships']['status']) {
      return;
    }

    $group_types = array_filter($values['managed_memberships']['group_types']);
    if (!$group_types) {
      $form_state->setErrorByName(
        'group_types',
        $this->t(
          'You must select at least one group type to enable group membership
           management for.'
        )
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('commerceg_product_group.settings');

    // @I Prevent disabling managed memberships when there already exist some
    //    type     : bug
    //    priority : low
    //    labels   : config, managed-memberships
    //    notes    : We should first review what disabling would mean if managed
    //               memberships already exist e.g. do we remove the
    //               memberships, and do we remove the group role as well? Do we
    //               force a clean up before permitting disabling the feature?
    if (!$values['managed_memberships']['status']) {
      $config->set('managed_memberships.status', FALSE)->save();
      parent::submitForm($form, $form_state);
      return;
    }

    $this->installer->installManagedMembershipRoles();

    // Install product group associations for group types that are added for the
    // first time.
    $group_types = array_diff(
      array_filter($values['managed_memberships']['group_types']),
      $config->get('managed_memberships.group_types')
    );
    foreach ($group_types as $group_type_id) {
      $group_type = $this->groupTypeStorage->load($group_type_id);
      $this->installer->installProductGroupContentType($group_type);
      $this->configurator->configureProductGroupContentType($group_type);
    }

    $config
      ->set('managed_memberships.status', TRUE)
      ->set(
        'managed_memberships.group_types',
        array_values($values['managed_memberships']['group_types'])
      )
      ->save();

    parent::submitForm($form, $form_state);
  }

}
