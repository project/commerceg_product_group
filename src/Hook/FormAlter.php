<?php

namespace Drupal\commerceg_product_group\Hook;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerceg_product_group\Configure\ConfiguratorInterface;
use Drupal\commerceg_product_group\Configure\InstallerInterface;
use Drupal\commerceg_product_group\Group\ContentLoaderInterface;

use Drupal\Core\Entity\ContentEntityFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Holds methods implementing form alteration hooks.
 */
class FormAlter {

  use StringTranslationTrait;

  /**
   * The Product Group module configurator.
   *
   * @var \Drupal\commerceg_product_group\Configure\ConfiguratorInterface
   */
  protected $configurator;

  /**
   * The Product Group content loader.
   *
   * @var \Drupal\commerceg_product_group\Group\ContentLoaderInterface
   */
  protected $contentLoader;

  /**
   * The Product Group module installer.
   *
   * @var \Drupal\commerceg_product_group\Configure\InstallerInterface
   */
  protected $installer;

  /**
   * Constructs a new FormAlter object.
   *
   * @param \Drupal\commerceg_product_group\Configure\ConfiguratorInterface $configurator
   *   The Product Group module configurator.
   * @param \Drupal\commerceg_product_group\Configure\InstallerInterface $installer
   *   The Product Group module installer.
   * @param \Drupal\commerceg_product_group\Group\ContentLoaderInterface $content_loader
   *   The product group content loader.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(
    ConfiguratorInterface $configurator,
    InstallerInterface $installer,
    ContentLoaderInterface $content_loader,
    TranslationInterface $string_translation
  ) {
    $this->configurator = $configurator;
    $this->installer = $installer;
    $this->contentLoader = $content_loader;

    // Properties defined in traits.
    $this->stringTranslation = $string_translation;
  }

  /**
   * Form alterations for the product add/edit form.
   *
   * Adds display of the Products groups that the product belongs to.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @I Add ability to add/edit Products groups in the product form
   *    type     : feature
   *    priority : normal
   *    labels   : product-group, ux
   * @I Add the Products groups pane on the add product form as well
   *    type     : feature
   *    priority : normal
   *    labels   : product-group, ux
   */
  public function productFormAlter(
    array &$form,
    FormStateInterface $form_state
  ) {
    $form_object = $form_state->getFormObject();
    if (!$form_object instanceof ContentEntityFormInterface) {
      return;
    }
    if ($form_object->getBaseFormId() !== 'commerce_product_form') {
      return;
    }

    $product = $form_object->getEntity();
    if (!$product->isNew()) {
      $this->editProductFormAlter($form, $form_state, $product);
    }
  }

  /**
   * Implements hook_form_FORM_ID_alter().
   *
   * Handles Product Group integration settings for product types. We add a
   * checkbox for enabling/disabling integration for the product type being
   * created/edited.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function productTypeFormAlter(
    array &$form,
    FormStateInterface $form_state
  ) {
    $product_type = $form_state->getFormObject()->getEntity();

    $description = $this->t(
      'When enabled, products of this type can be added to Product Groups.'
    );

    $disabled = FALSE;
    $can_uninstall = NULL;
    $is_installed = $this->installer->isProductTypeInstalled($product_type);
    if ($is_installed) {
      $can_uninstall = $this->installer->canUninstallProductType($product_type);
    }

    if ($is_installed && !$can_uninstall) {
      $disabled = TRUE;
      $description .= '<p><em>' . $this->t(
        'You cannot remove availability of this product type from Product Groups
         because there already exist products of this type that belong to
         Product Groups.'
      ) . '</em></p>';
    }

    $form['commerceg_product_group_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Make products available to Product Groups'),
      '#description' => $description,
      '#default_value' => $is_installed,
      '#disabled' => $disabled,
    ];

    // We do not add validate/submit callbacks in this class to avoid mixing
    // static with non-static methods; that would create issues with writing
    // automated tests.
    $form['actions']['submit']['#validate'][] = '\Drupal\commerceg_product_group\Form\Callbacks::productTypeValidate';
    $form['actions']['submit']['#submit'][] = '\Drupal\commerceg_product_group\Form\Callbacks::productTypeSubmit';
  }

  /**
   * Form alterations for the product edit form.
   *
   * Adds display of the Products groups that the Product belongs to.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param \Drupal\commerce_product\Entity\ProductInterface $product
   *   The product being edited.
   */
  protected function editProductFormAlter(
    array &$form,
    FormStateInterface $form_state,
    ProductInterface $product
  ) {
    $contents = $this->contentLoader->loadForProduct($product);

    $form['commerceg_product_group'] = [
      '#type' => 'details',
      '#title' => $this->t('Product groups'),
      '#open' => TRUE,
      '#group' => 'advanced',
    ];
    // Products groups determine product access and therefore visibility. It
    // makes sense to place this form element group just after the visibility
    // settings element group.
    if (isset($form['visibility_settings']['#weight'])) {
      $form['commerceg_product_group']['#weight'] = $form['visibility_settings']['#weight'] + 1;
    }
    if (!$contents) {
      $form['commerceg_product_group']['message'] = [
        // @I Add link to the product groups listing
        '#plain_text' => $this->t(
          'This product is not content of any product groups yet.'
        ),
      ];
      return;
    }

    $links = array_map(
      function ($content) {
        $group = $content->getGroup();
        return [
          '#type' => 'link',
          '#title' => $group->label(),
          '#url' => $group->toUrl(),
        ];
      },
      $contents
    );
    $form['commerceg_product_group']['groups'] = [
      '#type' => 'inline_template',
      '#template' => '<ul>{% for link in links %}<li>{{ link }}</li>{% endfor %}</ul>',
      '#context' => [
        'links' => $links,
      ],
    ];
  }

}
