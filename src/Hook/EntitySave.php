<?php

namespace Drupal\commerceg_product_group\Hook;

use Drupal\commerceg\MachineName\Plugin\GroupContentEnabler;
use Drupal\commerceg_product_group\MachineName\Bundle\Group as GroupBundle;
use Drupal\commerceg_product_group\MachineName\Plugin\GroupContentEnabler as ProductGroupGroupContentEnabler;
use Drupal\commerceg_product_group\Membership\ManagerInterface as MembershipManagerInterface;
use Drupal\group\Entity\GroupContentInterface;
use Drupal\group\Entity\GroupInterface;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Holds methods implementing hooks related to entity saving/deleting.
 */
class EntitySave {

  /**
   * The product group membership manager.
   *
   * @var \Drupal\commerceg_product_group\Membership\ManagerInterface
   */
  protected $membershipManager;

  /**
   * This module's configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a new EntitySave object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\commerceg_product_group\Membership\ManagerInterface $membership_manager
   *   The product group membership manager.
   *
   * @see \Drupal\commerceg_product_group\Membership\ManagerInterface::ensureMemberships()
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    MembershipManagerInterface $membership_manager
  ) {
    $this->config = $config_factory->get('commerceg_product_group.settings');
    $this->membershipManager = $membership_manager;
  }

  /**
   * Implements hook_ENTITY_TYPE_insert().
   *
   * @param \Drupal\group\Entity\GroupContentInterface $content
   *   The created group content entity.
   */
  public function groupContentInsert(GroupContentInterface $content) {
    $settings = $this->config->get('managed_memberships');
    if (!$settings['status']) {
      return;
    }

    $associated_group = $content->getGroup();
    if (!in_array($associated_group->bundle(), $settings['group_types'])) {
      return;
    }

    $this->associationContentInsert($content, $associated_group);
    $this->membershipContentInsert($content, $associated_group);
  }

  /**
   * Implements hook_ENTITY_TYPE_delete().
   *
   * @param \Drupal\group\Entity\GroupContentInterface $content
   *   The created group content entity.
   *
   * @see \Drupal\commerceg_product_group\Membership\ManagerInterface::removeMemberships()
   */
  public function groupContentDelete(GroupContentInterface $content) {
    $settings = $this->config->get('managed_memberships');
    if (!$settings['status']) {
      return;
    }

    $associated_group = $content->getGroup();
    if (!in_array($associated_group->bundle(), $settings['group_types'])) {
      return;
    }

    $this->associationContentDelete($content, $associated_group);
    $this->membershipContentDelete($content, $associated_group);
  }

  /**
   * Adds group memberships for all group users for new associations.
   *
   * When a product group is associated with a group, we add all group members
   * to the product group with the Group Customer group role.
   *
   * @param \Drupal\group\Entity\GroupContentInterface $content
   *   The group content describing the membership.
   * @param \Drupal\group\Entity\GroupInterface $associated_group
   *   The associated group.
   */
  protected function associationContentInsert(
    GroupContentInterface $content,
    GroupInterface $associated_group
  ) {
    $plugin_id = $content->getContentPlugin()->getPluginId();
    if ($plugin_id !== ProductGroupGroupContentEnabler::PRODUCTS) {
      return;
    }

    $product_group = $content->getEntity();
    if ($product_group->getEntityTypeId() !== 'group') {
      return;
    }
    if ($product_group->bundle() !== GroupBundle::PRODUCTS) {
      return;
    }

    $this->membershipManager->ensureMembershipsForAssociatedGroup(
      $product_group,
      $associated_group
    );
  }

  /**
   * Adds group memberships when a user is added as member to a group.
   *
   * When a membership of a user in a group is created, we add memberships in
   * all product groups that are associated to the group.
   *
   * @param \Drupal\group\Entity\GroupContentInterface $content
   *   The group content describing the membership.
   * @param \Drupal\group\Entity\GroupInterface $associated_group
   *   The associated group.
   */
  protected function membershipContentInsert(
    GroupContentInterface $content,
    GroupInterface $associated_group
  ) {
    $plugin_id = $content->getContentPlugin()->getPluginId();
    if ($plugin_id !== GroupContentEnabler::MEMBERSHIP) {
      return;
    }

    $user = $content->getEntity();
    // The entity of a content can be missing if the group content is being
    // deleted as a result of the entity being deleted first.
    if (!$user) {
      return;
    }
    if ($user->getEntityTypeId() !== 'user') {
      return;
    }

    $this->membershipManager->ensureMembershipsForUser(
      $user,
      $associated_group
    );
  }

  /**
   * Removes group memberships when a product group association is removed.
   *
   * When an association between a product group and a group is removed, we
   * remove the product group memberships for all associated group members.
   *
   * @param \Drupal\group\Entity\GroupContentInterface $content
   *   The group content describing the association.
   * @param \Drupal\group\Entity\GroupInterface $associated_group
   *   The associated group.
   */
  protected function associationContentDelete(
    GroupContentInterface $content,
    GroupInterface $associated_group
  ) {
    $plugin_id = $content->getContentPlugin()->getPluginId();
    if ($plugin_id !== ProductGroupGroupContentEnabler::PRODUCTS) {
      return;
    }

    $product_group = $content->getEntity();
    // The entity of a content can be missing if the group content is being
    // deleted as a result of the entity being deleted first.
    if (!$product_group) {
      return;
    }
    if ($product_group->getEntityTypeId() !== 'group') {
      return;
    }
    if ($product_group->bundle() !== GroupBundle::PRODUCTS) {
      return;
    }

    $this->membershipManager->removeMembershipsForAssociatedGroup(
      $product_group,
      $associated_group
    );
  }

  /**
   * Removes group memberships when a user is removed from a group.
   *
   * When a user membership in a group is removed, we remove all memberships
   * that the user may have to any product group associated with the group.
   *
   * @param \Drupal\group\Entity\GroupContentInterface $content
   *   The group content describing the membership.
   * @param \Drupal\group\Entity\GroupInterface $associated_group
   *   The associated group.
   */
  protected function membershipContentDelete(
    GroupContentInterface $content,
    GroupInterface $associated_group
  ) {
    $plugin_id = $content->getContentPlugin()->getPluginId();
    if ($plugin_id !== GroupContentEnabler::MEMBERSHIP) {
      return;
    }

    $user = $content->getEntity();
    // The entity of a content can be missing if the group content is being
    // deleted as a result of the entity being deleted first.
    if (!$user) {
      return;
    }
    if ($user->getEntityTypeId() !== 'user') {
      return;
    }

    $this->membershipManager->removeMembershipsForUser(
      $user,
      $associated_group
    );
  }

}
