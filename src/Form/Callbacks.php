<?php

namespace Drupal\commerceg_product_group\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements callbacks for forms provided by other modules.
 *
 * Submit and validate callbacks are added here instead of module files for
 * better OOP. They are static methods as they are simple PHP callables and
 * not methods provided by services registered with the service container.
 *
 * We also do not add them to the form alter hook service because mixing static
 * with non-static methods on the same class can create problems is not a good
 * practice; it causes issues with automated tests.
 *
 * @I Consider using a render element
 *    type     : task
 *    priority : low
 *    labels   : coding-standards
 */
class Callbacks {

  /**
   * Validates product group integration for the submitted product type.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public static function productTypeValidate(
    array $form,
    FormStateInterface $form_state
  ) {
    $product_type = $form_state->getFormObject()->getEntity();
    $values = $form_state->getValues();

    $installer = \Drupal::service('commerceg_product_group.installer');
    $is_installed = $installer->isProductTypeInstalled($product_type);

    $can_uninstall = NULL;
    if ($is_installed) {
      $can_uninstall = $installer->canUninstallProductType($product_type);
    }

    if (!$values['commerceg_product_group_status'] && $is_installed && !$can_uninstall) {
      $form_state->setErrorByName(
        'commerceg_product_group_status',
        t(
          'You cannot remove availability of this product type from Product
           Groups because there already exist products of this type that belong
           to Product Groups.'
        )
      );
    }
  }

  /**
   * Handles Product Group integration for the submitted product type.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public static function productTypeSubmit(
    array $form,
    FormStateInterface $form_state
  ) {
    $configurator = \Drupal::service('commerceg_product_group.configurator');
    $installer = \Drupal::service('commerceg_product_group.installer');
    $messenger = \Drupal::service('messenger');

    $product_type = $form_state->getFormObject()->getEntity();
    $values = $form_state->getValues();

    $is_installed = $installer->isProductTypeInstalled($product_type);

    if ($values['commerceg_product_group_status'] && !$is_installed) {
      $installer->installProductType($product_type);
      $configurator->configureProductType($product_type);
      $messenger->addStatus(t(
        'The %product_type product type has been successfully installed on
         Product Groups.',
        ['%product_type' => $product_type->label()]
      ));
    }
    elseif (!$values['commerceg_product_group_status'] && $is_installed) {
      $installer->uninstallProductType($product_type);
      $messenger->addStatus(t(
        'The %product_type product type has been successfully uninstalled from
         Product Groups.',
        ['%product_type' => $product_type->label()]
      ));
    }
  }

}
