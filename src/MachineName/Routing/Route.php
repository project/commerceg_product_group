<?php

namespace Drupal\commerceg_product_group\MachineName\Routing;

/**
 * Holds IDs of routes.
 *
 * See https://github.com/krystalcode/drupal8-coding-standards/blob/master/Fields.md#field-name-constants
 */
class Route {

  /**
   * Holds the name of the route that lists all product groups.
   */
  const ALL_PRODUCT_GROUPS = 'view.commerceg_product_groups.page_1';

  /**
   * Holds the name of the route that lists a group's product groups.
   */
  const GROUP_PRODUCT_GROUPS = 'view.commerceg_group_product_groups.page_1';

}
