<?php

namespace Drupal\commerceg_product_group\MachineName\Bundle;

/**
 * Holds machine names of Group entity bundles.
 *
 * See https://github.com/krystalcode/drupal8-coding-standards/blob/master/Fields.md#field-name-constants
 */
class Group {

  /**
   * Holds the machine name for the Products group type.
   */
  const PRODUCTS = 'commerceg_products';

}
