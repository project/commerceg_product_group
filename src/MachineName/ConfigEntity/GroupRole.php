<?php

namespace Drupal\commerceg_product_group\MachineName\ConfigEntity;

/**
 * Holds IDs of Group Role entities.
 *
 * See https://github.com/krystalcode/drupal8-coding-standards/blob/master/Fields.md#field-name-constants
 */
class GroupRole {

  /**
   * Holds the ID of the anonymous product group role.
   */
  const ANONYMOUS = 'commerceg_products-anonymous';

  /**
   * Holds the ID of the customer group role.
   *
   * The customer role is given when individual customers are added to product
   * groups so that they have access to view products.
   */
  const CUSTOMER = 'commerceg_products-customer';

  /**
   * Holds the ID of the group customer group role.
   *
   * The group customer role is given automatically to product group members
   * added via the managed memberships feature i.e. on behalf of other groups
   * that are associated with product groups.
   */
  const GROUP_CUSTOMER = 'commerceg_products-gcustomer';

  /**
   * Holds the ID of the group role given to product group manager users.
   */
  const MANAGER = 'commerceg_products-manager';

  /**
   * Holds the ID of the member product group role.
   */
  const MEMBER = 'commerceg_products-member';

  /**
   * Holds the ID of the outsider product group role.
   */
  const OUTSIDER = 'commerceg_products-outsider';

}
