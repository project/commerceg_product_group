<?php

namespace Drupal\commerceg_product_group\MachineName\Plugin;

/**
 * Holds machine names of Group Content Enabler plugins.
 *
 * See https://github.com/krystalcode/drupal8-coding-standards/blob/master/Fields.md#field-name-constants
 */
class GroupContentEnabler {

  /**
   * Holds the machine name for the Products enabler plugin.
   *
   * It allows associating product groups with other groups such as
   * organizations (provided by the Commerce B2B module) or customer groups
   * (provided by the Commerce Customer Group module).
   */
  const PRODUCTS = 'commerceg_products';

}
